﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaggedCategories.Domain
{
    public class Tag: BaseEntity<int>
    {
        public string Name { get; set; }
    }
}
