﻿using System;

namespace TaggedCategories.Domain
{
    public class Category : BaseEntity<int>
    {
        public string Name { get; set; }
        public string Tags { get; set; }
    }
}
