﻿
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TaggedCategories.Domain
{
    public interface IRepository<TEntity,TPrimary> where TEntity:BaseEntity<TPrimary>
    {
        public Task<TPrimary> CreateAsync(TEntity entity);
        public Task<TEntity> GetAsync(TPrimary Id);
        Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predictate);
        public Task<List<TEntity>> GetAllAsync();
        public Task<bool> UpdateAsync(TEntity entity);
        public Task<bool> DeleteAsync(TEntity entity);
        public Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate);
    }
}
