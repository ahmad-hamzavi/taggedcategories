﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaggedCategories.Domain
{
    public class BaseEntity<TPrimary>
    {
        public TPrimary Id { get; set; }
    }
}
