﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaggedCategories.Domain;

namespace TaggedCategories.Data
{
    public class CategoryService : CategoryRepository
    {
        public TagService _tagService { get; }

        public CategoryService(ApplicationDBContext context, TagService tagService) : base(context)
        {
            _tagService = tagService;
        }
        public async Task<Category> CreateAsync(string name)
        {
            if (name is null)
            {
                throw new ArgumentNullException("Name is Empty.");
            }

            Category category = new Category() { Name = name };
            _context.Categories.Add(category);
            await _context.SaveChangesAsync();
            return category;
        }

        public async Task<Category> AddTag(int categotyId, int? tagId)
        {
            if (tagId is null)
            {
                throw new ArgumentNullException("Please provide a valid Tag");
            }
            var tag = await _tagService.GetAsync(tagId.Value);
            if (tag is null)
            {
                throw new ArgumentNullException("Please provide a valid Tag");
            }
            var category = await GetAsync(categotyId);
            if (category is null)
            {
                throw new ArgumentNullException("Please provide a valid Category");
            }
            if (!string.IsNullOrWhiteSpace(category.Tags))
            {
                if (category.Tags.Contains($"#{tag.Name},"))
                {
                    throw new ArgumentNullException("Category already has this tag");
                }
                else if (category.Tags.Contains($"#{tag.Name}_"))
                {
                    throw new ArgumentNullException("Category already is child of this tag");
                }
                else if (tag.Name.Contains("_"))
                {
                    var tagName = tag.Name.Split("_").Last();
                    var parentTagName = tag.Name.Substring(0, tag.Name.IndexOf($"_{tagName}"));
                    if (category.Tags.Contains($"#{parentTagName},"))
                    {
                        throw new ArgumentNullException("Category already is child of this tag");
                    }
                    else if (category.Tags.Contains($"#{parentTagName}_"))
                    {
                        throw new ArgumentNullException("Category already has parent of this tag");
                    }
                }
            }
            category.Tags = category.Tags is null ? $"#{tag.Name}," : $"{category.Tags}#{tag.Name},";
            if (await UpdateAsync(category))
            {
                return category;
            }
            throw new ArgumentNullException("Error when Updadating Category");
        }

        public async Task<List<Category>> GetSubCategories(int categotyId)
        {
            var categories = new List<Category>();
            var category = await GetAsync(categotyId);
            if (category is null)
            {
                throw new ArgumentNullException("Please provide a valid Category");
            }
            if (!string.IsNullOrWhiteSpace(category.Tags))
            {
                var tags = category.Tags.Contains(",") ? category.Tags.Split(",").ToList() : new List<string> { category.Tags };
                tags.RemoveAll(x => x == "");
                foreach (var tag in tags)
                {                   
                    categories.AddRange(await GetAllAsync(x => x.Tags.Contains($"{tag}_")));
                }
            }
            return categories;
        }
    }
}
