﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TaggedCategories.Domain;

namespace TaggedCategories.Data
{
    public class TagRepository : IRepository<Tag, int>
    {
        private ApplicationDBContext _context;
        public TagRepository(ApplicationDBContext context)
        {
            _context = context;
        }
        public async Task<int> CreateAsync(Tag tag)
        {
            if (tag is null)
            {
                throw new ArgumentNullException(nameof(tag));
            }

            _context.Tags.Add(tag);
            await _context.SaveChangesAsync();
            return tag.Id;
        }

        public async Task<bool> DeleteAsync(Tag tag)
        {
            if (tag is null)
            {
                throw new ArgumentNullException(nameof(tag));
            }

            _context.Tags.Remove(tag);
            var result = await _context.SaveChangesAsync();
            return result > 0;
        }

        public async Task<Tag> GetAsync(int Id)
        {
            return await _context.Tags.FindAsync(Id);
        }

        public async Task<List<Tag>> GetAllAsync()
        {
            return await _context.Tags.ToListAsync();
        }

        public async Task<bool> UpdateAsync(Tag tag)
        {
            _context.Update(tag);
            var result = await _context.SaveChangesAsync();
            return result > 0;
        }

        public async Task<int> CountAsync(Expression<Func<Tag, bool>> predicate)
        {
           return await _context.Tags.CountAsync(predicate);
        }

        public async Task<List<Tag>> GetAllAsync(Expression<Func<Tag, bool>> predictate)
        {
            return await _context.Tags.Where(predictate).ToListAsync();
        }
    }
}
