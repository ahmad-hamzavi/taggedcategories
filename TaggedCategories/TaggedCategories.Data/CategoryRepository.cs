﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TaggedCategories.Domain;

namespace TaggedCategories.Data
{
    public class CategoryRepository : IRepository<Category, int>
    {
        protected ApplicationDBContext _context;
        public CategoryRepository(ApplicationDBContext context)
        {
            _context = context;
        }
        public async Task<int> CreateAsync(Category category)
        {
            _context.Categories.Add(category);
            await _context.SaveChangesAsync();
            return category.Id;
        }

        public async Task<bool> DeleteAsync(Category category)
        {
            _context.Categories.Remove(category);
            var result = await _context.SaveChangesAsync();
            return result > 0;
        }

        public async Task<Category> GetAsync(int Id)
        {
            return await _context.Categories.FindAsync(Id);
        }
        public async Task<List<Category>> GetAllAsync(Expression<Func<Category,bool>> predictate)
        {
            return await _context.Categories.Where(predictate).ToListAsync();
        }
        public async Task<List<Category>> GetAllAsync()
        {
            return await _context.Categories.ToListAsync();
        }

        public async Task<bool> UpdateAsync(Category category)
        {
            _context.Update(category);
            var result = await _context.SaveChangesAsync();
            return result > 0;
        }

        public async Task<int> CountAsync(Expression<System.Func<Category, bool>> predicate)
        {
          return  await _context.Categories.CountAsync(predicate);
        }
    }
}
