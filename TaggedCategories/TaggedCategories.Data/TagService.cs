﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaggedCategories.Domain;

namespace TaggedCategories.Data
{
    public class TagService : TagRepository
    {
        public TagService(ApplicationDBContext context) : base(context)
        {
        }
        public async Task<Tag> CreateTag(string name, int? parentTagId)
        {
            Tag parent = null;
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new System.ArgumentNullException("Name is empty.");
            }
            else if (name.Contains("_"))
            {
                throw new System.ArgumentNullException("_ is not a valid caracter.");
            }

            if (!(parentTagId is null))
            {
                parent = await GetAsync(parentTagId.Value);
            }
            Tag tag = new Tag();
            tag.Name = parent is null ? name : $"{parent.Name}_{name}";
            if ((await CountAsync(x => x.Name == tag.Name)) > 0)
            {
                throw new System.ArgumentNullException("Tag is already exist.");
            }
            if (await CreateAsync(tag) > 0)
            {
                return tag;
            }
            throw new System.ArgumentNullException("Error");
        }
    }
}
