﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaggedCategories.Data;
using TaggedCategories.Domain;

namespace TaggedCategories.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        public CategoryService _categoryService { get; }
        public CategoryController(CategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        /// <summary>
        /// Create new category
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Category> CreateCategory(string name)
        {
            return await _categoryService.CreateAsync(name);
        }
        /// <summary>
        /// Set tag to specefic category
        /// </summary>
        /// <param name="categotyId"></param>
        /// <param name="tagId"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<Category> AddTag(int categotyId, int? tagId)
        {
            return await _categoryService.AddTag(categotyId, tagId);
        }
        /// <summary>
        /// Get all related categories
        /// </summary>
        /// <param name="categotyId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Category>> GetSubCategories(int categotyId)
        {
            return await _categoryService.GetSubCategories(categotyId);
        }
    }
}
