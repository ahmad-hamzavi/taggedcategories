﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaggedCategories.Data;
using TaggedCategories.Domain;

namespace TaggedCategories.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagController : ControllerBase
    {
        public TagService _tagService { get; }
        public TagController(TagService tagService)
        {
            _tagService = tagService;
        }

        /// <summary>
        /// Get All tags
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Tag>> GetTags()
        {
            return await _tagService.GetAllAsync();
        }
        /// <summary>
        /// Create new tag 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parentTagId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Tag> CreateTag(string name,int? parentTagId)
        {
            return await _tagService.CreateTag(name,parentTagId);
        }
    }
}
